import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.salim.firechatApp',
  appName: 'fire-chat-app',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
