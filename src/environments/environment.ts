// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";

export const environment = {
  production: false,
  firebaseConfig: {
  apiKey: "AIzaSyCGu4j6MB6vA0IN14VlaU5eSx8yvRrF3Tc",
  authDomain: "chat-app-bb0f8.firebaseapp.com",
  databaseURL: "https://chat-app-bb0f8-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "chat-app-bb0f8",
  storageBucket: "chat-app-bb0f8.appspot.com",
  messagingSenderId: "181342781216",
  appId: "1:181342781216:web:b0fde1f5401b46b45ce145",
  measurementId: "G-GK4D3DQNZG"
}
};

const app = initializeApp(environment.firebaseConfig);
const analytics = getAnalytics(app);

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// im
