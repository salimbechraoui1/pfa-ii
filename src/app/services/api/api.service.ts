import { Injectable } from '@angular/core';
import {doc,Firestore,setDoc,getDoc, collectionData, getDocs, addDoc,  docData, orderBy} from '@angular/fire/firestore';
import { collection, OrderByDirection, query, where } from 'firebase/firestore';
import { ColdObservable } from 'rxjs/internal/testing/ColdObservable';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private firestore: Firestore) { }
  docRef(path) {
    return doc(this.firestore, path);
  }
  collectionRef(path) {
    return collection(this.firestore, path);
  }
  setDocument(path, data) {
    const dataRef = this.docRef(path);
    return setDoc(dataRef, data);
  }
  addDocument(path,data) {
    const dataRef = this.collectionRef(path);
    return addDoc(dataRef, data);
  }
  getDocById(path) {
    const dataRef = this.docRef(path);
    return getDoc(dataRef) ;
  }

  getDocs(path, queryFn?) {
    let dataRef: any =this.collectionRef(path) ;
    if(queryFn) {
      const q =query(dataRef, queryFn) ;
      dataRef = q; 

    }
    return getDocs(dataRef) ;
  }
   collectionDataQuery(path,queryFn?) {
    let dataRef: any = this.collectionRef(path);
    if(queryFn) {
      const q = query(dataRef,queryFn) ;
      dataRef = q ;
    }
   const  collection_data = collectionData<any>(dataRef , { idField: 'id'});
    return collection_data ;
   }
   whereQuery(fieldPath, condition , value ) {
    return where(fieldPath, condition, value) ;
   }
   
   orderByQuery(fieldPath, directionStr: OrderByDirection = 'asc') {
     return orderBy(fieldPath, directionStr) ;

   }
   docDataQuery(path, id?,queryFn?) {
    let dataRef: any = this.docRef(path);
    if(queryFn) {
       const q = query(dataRef,queryFn) ;
       dataRef = q ;

    }
    let doc_data; 
    if(id) doc_data = docData<any>(dataRef, {idField:'id'}) ;
    else doc_data = docData<any>(dataRef);
    return doc_data;
   }

}