import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { provideFirebaseApp, initializeApp, getApp } from '@angular/fire/app';
import { provideFirestore, getFirestore } from '@angular/fire/firestore';
import { IonicModule } from '@ionic/angular';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouteReuseStrategy } from '@angular/router';
import { IonicRouteStrategy } from '@ionic/angular';
import { environment } from '../environments/environment';
import  {provideAuth,getAuth} from '@angular/fire/auth'
@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    provideFirebaseApp(() => initializeApp(environment.firebaseConfig)),
    provideAuth(() => getAuth() ) ,
    provideFirestore(() => getFirestore())
  ],   
  providers: [
    {
    provide: RouteReuseStrategy, useClass: IonicRouteStrategy }]
    ,
  bootstrap: [AppComponent]
})
export class AppModule { }
