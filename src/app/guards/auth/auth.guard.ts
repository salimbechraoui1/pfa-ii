import { Injectable } from "@angular/core";
import { CanLoad, Router } from "@angular/router";
import { AuthService } from "src/app/services/auth/auth.service";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanLoad {
  constructor(
    private authService: AuthService, 
    private router: Router 
  ) {}

  async canLoad(): Promise<boolean> {
    try {
      const user = await this.authService.checkAuth();
      console.log(user);
      if(user) {
        return true;
      } else {
        this.navigate('/login');
        return false; // Return false here to prevent loading the module
      }
    } catch (e) {
      console.log(e);
      this.navigate('/login');
      return false; // Return false here to prevent loading the module
    }
  }

  navigate(url: string | any[]) {
    if (typeof url === 'string') {
      url = [url]; // Convert to array if it's a string
    }
    this.router.navigate(url, { replaceUrl: true });
  }
}