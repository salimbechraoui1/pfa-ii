import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { LoginPageRoutingModule } from './login-routing.module';

import { LoginPage } from './login.page';
import { RouterModule, Routes } from '@angular/router';
import { HomePage } from '../home/home.page';
import { IonicModule } from '@ionic/angular';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule ,
    IonicModule ,
    LoginPageRoutingModule
  ],

  declarations: [LoginPage]
})

export class LoginPageModule {}
