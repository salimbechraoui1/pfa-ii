import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from './../../../services/auth/auth.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  signupForm: FormGroup;
  isTypePassword: boolean = true;
  isLoading: boolean = false;

  constructor(
    private router: Router,
    private authService: AuthService,
    private alertController: AlertController
  ) {
    this.initForm();
  }

  ngOnInit() {}

  initForm() {
    this.signupForm = new FormGroup({
      username: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(8)]),
    });
  }

  onChange() {
    this.isTypePassword = !this.isTypePassword;
  }

  async onSubmit() {
    if (!this.signupForm.valid) return;
    console.log(this.signupForm.value);
    try {
      this.isLoading = true;
      const data = await this.authService.register(this.signupForm.value);
      console.log(data);
      this.router.navigateByUrl('/home');
      this.isLoading = false;
      this.signupForm.reset();
    } catch (error) {
      console.error(error);
      let msg: string = 'Could not sign you up, please try again.';
      if (error.code == 'auth/email-already-in-use') {
        msg = 'Email already in use';
      }
      await this.presentAlert(msg);
      this.isLoading = false;
    }
  }

  async presentAlert(msg) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: msg,
      buttons: ['OK'],
    });
    await alert.present();
  }
}
