import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage  {

  form: FormGroup;
  isTypePassword: boolean = true;
  isLogin = false;

  constructor(
    private router: Router,
    private authService: AuthService,
    private alertController: AlertController
  ) {
    this.initForm();
  }


  initForm() {
    this.form = new FormGroup({
      email: new FormControl('', 
        {validators: [Validators.required, Validators.email]}
      ),
      password: new FormControl('', 
        {validators: [Validators.required, Validators.minLength(8)]}
      ),
    });
  }

  onChange() {
    this.isTypePassword = !this.isTypePassword;
  }

  onSubmit() {
    if(!this.form.valid) return;
    console.log(this.form.value);
    this.login(this.form);
  }
  login(form) {
    this.authService.login(form.value.email, form.value.password).then(data => {
      console.log(data);
      this.router.navigateByUrl('/home') ;
      //this.global.hideLoader();
      this.form.reset() ;
    })
    .catch(e => {
      console.log(e);
      //this.global.hideLoader() ;
      let msg: string = 'Could not sign u in , please try again';
      if(e.code =='auth/user-not-found') msg ='E-mail address cloud not found';
      else if(e.code =="auth/wrong-password") msg= ' please enter a correct password' ;
      this.presentAlert(msg) ;


  });

 }
 async presentAlert(msg) {
  const alert = await this.alertController.create({
    header: 'Alert',
    message: msg,
    buttons: ['OK'],
  });
  await alert.present();
}
}